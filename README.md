#None of the below features are implemented, this is still in developmental stage, please be sure to checkout later.

#Chatbot-Constructor

##Description

A chatbot constructor that can help you develop chatbots in no time either using javascript and nodejs or without programming using excel sheet or JSON file. 
And it can also easily integrate with AI services like LUIS, Watson, Lex, Octane.AI, Wit.AI, Api.AI and Recast.AI. 
And also supports multiple channels like website, facebook, slack, kik, telegram, discord, hololens, C# client, Java client, VR(using Unity), twilio, smooch and much more. 
It can also act as both a standalone chatbot for a non chatroom interface or as a moderator cum chatbot for a chatroom interface.
It has also got analytics and performance monitoring integrateed into the it with Custom UI which you can deploy anywhere and see the stats of your bot anytime.
Get feedback from Users with sentimental analysis to improve the bot.
Ability to log all the messages to any db such as mysql, sqlite, mongodb, postgres, rethinkdb or just a simple json file.